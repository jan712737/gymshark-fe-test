# List of things that need to be done next:

- [ ] Frontend design: Taking a look at the design specs and identify areas where we can make improvements to match the designs better. 
- [ ] Responsiveness and accessibility: Make sure the site looks good and functions well on different screen sizes and devices. Test the site with different assistive technologies (e.g., screen readers) to ensure that it is accessible to all users.
- [ ] Database integration: Decide on a database (e.g., MySQL, PostgreSQL, MongoDB) and integrate it with the system using an ORM like Prisma. Make sure to properly sanitize and validate user input to prevent security vulnerabilities like SQL injection attacks.
- [ ] Test coverage: Use Jest to write unit tests for the remaining components and ensure that code is functioning as expected. Increase coverage for the main screen.
- [ ] Better documentation: Implement more frontend documentation (storybook) for the components using features as knobs or variants to provide developers with better representation of what is available.
- [ ] E2E tests: Use tools like Cypress or Playwright to write end-to-end tests that simulate user interactions with the site. These tests can help catch bugs and regressions before they make it to production and ensure consistency throught development.
- [ ] CMS: Add a content management system for managing the contents of the site (something like storyblok, contentful or any other headless cms).
- [ ] Improve Performance: Consider implementing caching techniques and explore options like Server-Side Rendering (SSR) or Static Generation to improve website performance. Also, explore options like on-demand revalidation from NextJS.
- [ ] Linting issues: Fix any linting issues picked up by ESLint and other linting tools
- [ ] Functionality: Extend the functionality of the site based on user feedback or business requirements. This could include adding new features or integrating with third-party APIs. Making sure to test any new functionality thoroughly to ensure that it is working as expected.