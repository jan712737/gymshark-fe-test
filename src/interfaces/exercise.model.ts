export default interface Exercise {
  id: string
  name: string
  transcript: string
  female: {
    image: string
  }
  male: {
    image: string
  }
  bodyAreas: string[]
}

export default interface ExercisesData {
  exercises: Exercise[]
}
