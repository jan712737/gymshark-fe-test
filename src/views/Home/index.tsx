import React from 'react'
import ALink from 'components/ALink'
import Image from 'next/image'

import manifest from '../../../public/manifest.json'
const Home = ({
  title = manifest.name,
  description = manifest.description
}) => (
  <>
    <Image src="/img/logo.svg" width={50} height={50} alt="Gymshark Logo" />
    <h1>{title}</h1>
    <h3>{description}</h3>
    <ALink href="/exercises" passHref>
      <button>Exercises</button>
    </ALink>
  </>
)

export default Home
