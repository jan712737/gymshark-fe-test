import React from 'react'
import ALink from 'components/ALink'
import Image from 'next/image'

const NotFound = () => (
  <>
    <Image src="/img/logo.svg" width={50} height={50} alt="Gymshark Logo" />
    <h1>404 Not found!</h1>
    <div>
      Oops! The page you&apos;re looking for isn&apos;t here. Please try again
      or contact us for assistance.
    </div>
    <ALink href="/" passHref>
      <button>Home</button>
    </ALink>
  </>
)

export default NotFound
