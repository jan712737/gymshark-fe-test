import React from 'react'
import { useState, useEffect } from 'react'

import ExerciseData from 'interfaces/exercise.model'
import Image from 'next/image'

import ExerciseService from '../../services/api'
function Exercises() {
  const [exercisesData, setExercisesData] = useState<ExerciseData>()
  const [isMaleImage, setIsMaleImage] = useState<boolean>(true)

  useEffect(() => {
    async function fetchExercises() {
      const fetchedExercises = await ExerciseService.getExercises()

      if (fetchedExercises) {
        setExercisesData(fetchedExercises)
      }
    }
    fetchExercises()
  }, [])

  return (
    <div>
      <h1>Exercises</h1>
      <div>
        <button onClick={() => setIsMaleImage(!isMaleImage)}>
          {isMaleImage ? 'Show Female Images' : 'Show Male Images'}
        </button>
        {exercisesData?.exercises &&
          exercisesData?.exercises.map((exercise) => (
            <div key={exercise.id}>
              <h2>{exercise.name}</h2>
              {/* <div dangerouslySetInnerHTML={{ __html: exercise.transcript }}></div> */}
              <Image
                src={isMaleImage ? exercise.male.image : exercise.female.image}
                alt={exercise.name}
                width={150}
                height={100}
              />
              <p>Body Areas: {exercise.bodyAreas.join(', ')}</p>
            </div>
          ))}
      </div>
    </div>
  )
}

export default Exercises
