import ExercisesData from '../interfaces/exercise.model'

export async function fetchData<T>(
  url: string,
  options: RequestInit = {}
): Promise<T> {
  const response = await fetch(url, options)
  const data = (await response.json()) as T
  return data
}

export async function getExercises(): Promise<ExercisesData> {
  const data = await fetchData<ExercisesData>(
    'https://private-922d75-recruitmenttechnicaltest.apiary-mock.com/customexercises/'
  )
  return data
}

export default {
  fetchData,
  getExercises
}
