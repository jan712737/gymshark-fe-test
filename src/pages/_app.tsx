import React from 'react'
import { ToastContainer } from 'react-toastify'

import { AppProps } from 'next/app'
import Head from 'next/head'
import { ThemeProvider } from 'styled-components'
import { GlobalStyles, theme } from 'styles/global'

import 'react-toastify/dist/ReactToastify.css'
import manifest from '../../public/manifest.json'

function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
        <meta name="theme-color" content={manifest.theme_color} />

        <title>{manifest.name}</title>
        <meta name="title" content={manifest.name} />
        <meta name="description" content={manifest.description} />

        <meta property="og:type" content="website" />
        <meta property="og:url" content={manifest.public_url} />
        <meta property="og:site_name" content={manifest.name} />
        <meta property="og:title" content={manifest.name} />
        <meta property="og:description" content={manifest.description} />
        <meta
          property="og:image"
          itemProp="image"
          content={manifest.thumbnail}
        />
        <meta property="og:locale" content="en" />

        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content={manifest.public_url} />
        <meta property="twitter:title" content={manifest.name} />
        <meta property="twitter:description" content={manifest.description} />
        <meta property="twitter:image" content={manifest.thumbnail} />

        <link rel="shortcut icon" href="/img/icon-512.png" />
        <link rel="apple-touch-icon" href="/img/icon-512.png" />
        <link rel="manifest" href="/manifest.json" />
      </Head>
      <GlobalStyles />
      <ToastContainer />
      <ThemeProvider theme={theme}>
        <Component {...pageProps} />
      </ThemeProvider>
    </>
  )
}

export default App
