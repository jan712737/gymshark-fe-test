import Link from 'next/link'
import styled from 'styled-components'

export const AlteredLink = styled(Link)`
  padding: 8px 10px;
  border-radius: 6px;
`
